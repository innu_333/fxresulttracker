(function () {
  // 元データ
  let originList = [];
  let chart = null;
  init();

  // タブ
  const tabsElement = document.getElementById('tabs');

  // 対象範囲の合計損益　要素
  const totalTargetElement = document.getElementById('total-target');
  const totalProfitAndLossElement = document.getElementById(
    'total-profit-and-loss'
  );
  const totalImgElement = document.getElementById('total-img');
  // モーダルの中身要素
  const modalContentElement = document.getElementById('modal-content');

  // 月間損益　要素
  const monthResultTbodyElement = document.getElementById('month-result-tbody');
  /**
   * 初期表示時の処理
   */
  function init() {
    // PapaParseを使用してCSVを読み込む
    Papa.parse('./trade_result.csv', {
      download: true,
      header: false,
      complete: function (results) {
        // ここで結果を処理する
        results.data.pop();
        originList = results.data;
        updateTab();

        // データをフィルター
        const tradeData = calcTotal();
        // 全データから年をフィルターする
        const yearList = Array.from(
          new Set(tradeData.dateList.map((item) => item.split('/')[0]))
        );
        console.log(yearList);
        yearList.forEach(function (elem) {
          tabsElement.innerHTML += `<li data-type="${elem}"><a href="#">${elem}年</a></li>`;
        });

        // タブが変更されたときの処理
        const liElements = document.querySelectorAll('#tabs li');
        let currentTabIndex = 0;
        liElements.forEach(function (elem, index) {
          const isActive = elem.classList.contains('uk-active');
          if (isActive) {
            currentTabIndex = index;
          }
          elem.addEventListener('click', function () {
            if (index !== currentTabIndex) {
              currentTabIndex = index;

              const type = elem.getAttribute('data-type');
              updateTab(type, false);
            }
          });
        });
      },
      error: function (error) {
        console.error('CSV読み込みエラー:', error.message);
      }
    });
  }

  // タブ切り替え時のデータ反映処理
  function updateTab(type = 'lifetime', isInit = true) {
    let totalTitle = '生涯損益';
    let chartTitle = '生涯損益 累計グラフ(円)';
    let imgUrls = ['./img/lifetime/fx1.png', './img/lifetime/fx2.png'];

    if (type !== 'lifetime') {
      totalTitle = `${type}年損益`;
      chartTitle = `${type}年損益 累計グラフ(円)`;
      imgUrls = [`./img/year/${type}.png`];
    }

    // データをフィルター
    const tradeData = calcTotal(type);

    // チャートに表示
    updateChart(tradeData.dateList, tradeData.totalList, chartTitle, isInit);

    // 対象範囲の合計損益 反映
    totalTargetElement.innerText = totalTitle;
    const lastTotal = tradeData.totalList[tradeData.totalList.length - 1];
    totalProfitAndLossElement.innerHTML = formatAmountStyle(lastTotal);

    let imgString = '';
    let buttonString = '';
    imgUrls.forEach(function (elem, index) {
      buttonString += `<button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-close-default" data-src="${elem}">画像を確認する</button></td>`;
    });
    totalImgElement.innerHTML = buttonString;
    // 証拠画像のボタンを押下した時に
    const imgButtonList1 = document.querySelectorAll('button[data-src]');
    imgButtonList1.forEach(function (buttonElement) {
      buttonElement.addEventListener('click', function () {
        const imgSrc = buttonElement.getAttribute('data-src');
        modalContentElement.innerHTML = `<img src="${imgSrc}" />`;
      });
    });

    // 月間損益を表示
    let monthTrString = '';
    for (let i = 0; i < tradeData.dateList.length; i += 1) {
      const imgSrcString = `./img/month/${tradeData.dateList[i].replace(
        '/',
        '-'
      )}.png`;
      monthTrString += `<tr>
        <td>${tradeData.dateList[i]}</td>
        <td>${formatAmountStyle(tradeData.amountList[i])}</td>
        <td><button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-close-default" data-src="${imgSrcString}">画像を確認する</button></td>
      </tr>`;
    }
    monthResultTbodyElement.innerHTML = monthTrString;
    // 証拠画像のボタンを押下した時に
    const imgButtonList2 = document.querySelectorAll('button[data-src]');
    imgButtonList2.forEach(function (buttonElement) {
      buttonElement.addEventListener('click', function () {
        const date = buttonElement.getAttribute('data-src');

        modalContentElement.innerHTML = `<img src="${date}" />`;
      });
    });
  }

  /**
   * 損益のスタイルを設定
   * マイナスの場合、赤字に変更する
   */
  function formatAmountStyle(amount) {
    if (amount < 0) {
      return `<span style="color: red;">${formatToYen(amount)}</span>`;
    }

    return formatToYen(amount);
  }

  /**
   * トレードデータをフィルターにかける
   */
  function calcTotal(type = 'lifetime') {
    const result = {
      amountList: [],
      dateList: [],
      totalList: []
    };

    let totalAmount = 0;
    for (let i = 0; i < originList.length; i++) {
      const date = originList[i][0];
      const value = parseInt(originList[i][1]);
      if (type === 'lifetime' || date.includes(type)) {
        // 合計を累積
        totalAmount += value;
        // 新しいデータを作成して追加
        result.totalList.push(totalAmount);
        result.amountList.push(value);
        result.dateList.push(date);
      }
    }
    return result;
  }

  /**
   * チャートを更新する関数
   */
  function updateChart(
    dates,
    totals,
    title = '生涯損益 累計グラフ(円)',
    isInit = true
  ) {
    const isNegative = totals[totals.length - 1] < 0;
    // チャートデータ
    const options = {
      series: [
        {
          name: '累計',
          data: totals
        }
      ],
      chart: {
        type: 'area',
        height: 450,
        width: '100%', // チャートの幅を100%に設定
        toolbar: {
          show: true // ツールバーを表示
        },
        zoom: {
          enabled: false
        }
      },
      tooltip: {
        y: {
          formatter: function (val) {
            // ツールチップの値に '¥' + val.toLocaleString('ja-JP') を適用
            return formatToYen(val);
          }
        }
      },
      dataLabels: {
        formatter: function (val, opts) {
          return formatToYen(val);
        }
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: title,
        align: 'center',
        style: {
          fontSize: '30' // タイトルのフォントサイズを30pxに変更
        }
      },
      labels: dates,
      xaxis: {
        type: 'string'
      },
      yaxis: {
        opposite: true,
        labels: {
          formatter: function (val) {
            return formatToYen(val);
          }
        }
      },
      legend: {
        horizontalAlign: 'left'
      },
      colors: [isNegative ? '#F44336' : '#3399FF']
    };
    if (isInit) {
      chart = new ApexCharts(document.querySelector('#chart'), options);
      chart.render();
    } else {
      chart.updateOptions(options);
    }
  }

  /**
   * 円記号 + 3桁毎にカンマを付ける
   */
  function formatToYen(value) {
    return '¥' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
})();
